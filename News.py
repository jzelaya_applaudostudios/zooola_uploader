class News(object):

    def __init__(self, columns):
        self.id = columns[0]
        self.date_added = columns[1]
        self.entered_by = columns[2]
        self.uploaded_date = columns[3]
        self.zooola_name = columns[4]
        self.display_name = columns[5]
        self.content = columns[6]
        self.image_name = columns[7]
        self.image_thumbnail = columns[8]
        self.push_group_category = columns[9]
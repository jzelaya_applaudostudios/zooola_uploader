import xlrd
import json
import time
from firebase import firebase as fb
from google.cloud import storage
from Audio import Audio
from Video import Video
from Narrations import Narrations
from Tutorial import Tutorial
from News import News
from User import User
from TextOverlayHeader import TextOverlayHeader
from TextOverlayItem import TextOverlayItem
from AdStyle import AdStyle

class Processor(object):

    def __init__(self, filePath):
        self.filePath = filePath
        self.book = xlrd.open_workbook(filePath)
        self.audioSheet = self.book.sheet_by_name("audios")
        self.videoSheet = self.book.sheet_by_name("videos")
        self.narrationSheet = self.book.sheet_by_name("narrations")
        self.tutorialSheet = self.book.sheet_by_name("tutorials")
        self.newsSheet = "" #self.book.sheet_by_name("news")
        self.userSheet = "" #self.book.sheet_by_name("users")
        self.txtHdSheet = ""#self.book.sheet_by_name("textoverlayheaders")
        self.txtItemSheet = self.book.sheet_by_name("textoverlayitems")
        self.adStyleSheet = self.book.sheet_by_name("adstyles")
        self.audio_list = []
        self.audioFilePaths = []
        self.initFireBase()

    def initFireBase(self):
        auth = fb.FirebaseAuthentication('UgewGa5Ss7yBBBRPqXBH2ZrrumdHMImrbgRE2qCV', #Settings -> service accounts -> database secrets
                                        'jzelaya@applaudostudios.com', 
                                        )
        # Enable realtime database
        self.firebase = fb.FirebaseApplication('https://zoola-f1c3d.firebaseio.com', authentication=auth)
        # Enable Storage to the specific proyect
        self.client = storage.Client('zoola')
        # Reference an existing bucket.
        self.bucket = self.client.get_bucket('zoola-f1c3d.appspot.com')

    def uploadTexts(self):
        self.uploadAssets("texts")

    def uploadAudios(self):
        self.uploadAssets("audios")

    def uploadVideos(self):
        self.uploadAssets("videos")

    def uploadNarrations(self):
        self.uploadAssets("narrations")

    def uploadTutorials(self):
        self.uploadAssets("tutorials")

    def uploadAdStyles(self):
        self.uploadAssets("adstyles")

    # Iterate through each row in worksheet and fetch values into object
    def extractContent(self, sheet):
        contenidoList = []
        paths = [] 
        for rownum in range(1, sheet.nrows):
            columns = sheet.row_values(rownum)                 
            contenido, path = self.buildObject(sheet, columns)
            contenidoList.append(contenido)
            paths.append(path)
        return contenidoList, paths

    def extractFilePaths(self, paths):
        extractedPaths = []
        for index, adStylePath in enumerate(paths, start=0):
            if isinstance(adStylePath, basestring) is False: #could be a path as string or an object containing multiple paths as strings
                for path in adStylePath:
                    extractedPaths.append(path)
            else:
                extractedPaths.append(adStylePath)
        return extractedPaths

    def buildObject(self, sheet, columns):
        contenido = ""
        path = ""
        if (sheet.name=="audios"):
            contenido = Audio(columns)
            path = columns[5]
        if (sheet.name=="videos"):
            contenido = Video(columns)
            path = columns[10]
        if (sheet.name=="narrations"):
            contenido = Narrations(columns)
            path = columns[5]
        if (sheet.name=="tutorials"):
            contenido = Tutorial(columns)
            path = columns[6]
        if (sheet.name=="adstyles"):
            contenido = AdStyle(columns)
            path = []
            path.append(columns[7])
            path.append(columns[10])
            path.append(columns[11])
        if (sheet.name=="news"):
            contenido = News(columns)
        if (sheet.name=="users"):
            contenido = User(columns)           
        if (sheet.name=="textoverlayitems"):
            contenido = TextOverlayItem(columns)            
        return contenido, path

    def getJsonAudios(self):
        return self.convertToJson(self.audio_list)

    def convertToJson(self, item):
        jsonStr = json.dumps(item.__dict__)
        jsonItem = json.loads(jsonStr)
        return jsonItem
    
    def uploadAssets(self, type):
        url = "/assets/" + type
        sheet = self.getSheet(type)
        item_list, itemFilePaths = self.extractContent(sheet)        
        print "uploading ", type
        total = len(item_list)
        for index, item in enumerate(item_list):
            print "uploading ", index + 1, " of ", total
            self.uploadAsJson(item, type)
            if "texts" != type:
                self.uploadAssetFile(item, type)            
        print type, "uploaded successfully"

    def getSheet(self, type):
        if "videos"==type:
            return self.videoSheet
        if "audios"==type:
            return self.audioSheet
        if "narrations"==type:
            return self.narrationSheet
        if "tutorials"==type:
            return self.tutorialSheet
        if "adstyles"==type:
            return self.adStyleSheet
        if "texts"==type:
            return self.txtItemSheet

    def uploadAsJson(self, item, type):    
        id = self.getId(item, type)    
        print "ID: ", id        
        jsonItem = self.convertToJson(item)        
        self.firebase.patch('/assets/'+type, {id : jsonItem})    
        print "json --> done"

    def getId(self, item, type):
        if "texts"==type:
            return item.id
        elif "adstyles"==type:
            return item.style
        else:
            return item.zooola_name

    def uploadAssetFile(self, item, type):
        if "adstyles"==type:
            print "uploading sample movie"
            zebraBlob = self.bucket.blob(item.sample_movie_name)
            zebraBlob.upload_from_filename(filename=item.sample_movie_name)
            print "uploading tab image"
            zebraBlob = self.bucket.blob(item.tab_image_name)
            zebraBlob.upload_from_filename(filename=item.tab_image_name)
            print "uploading selected tab image"
            zebraBlob = self.bucket.blob(item.tab_selected_image_name)
            zebraBlob.upload_from_filename(filename=item.tab_selected_image_name)
            print "assets --> done"            
        else:
            zebraBlob = self.bucket.blob(item.file_name)
            zebraBlob.upload_from_filename(filename=item.file_name)
            print "asset --> done"
    
    def convertTextToJson(self, items):
        jsonObjects = []
        for item in items:
            jsonStr = json.dumps(item.__dict__)
            jsonItem = json.loads(jsonStr)
            jsonObjects.append(jsonItem)
        return jsonObjects
class TextOverlayItem(object):

    def __init__(self, columns):        
        id = columns[0]
        if isinstance(id,float):
            id = int(id)
        self.id = id
        self.header_config_name = columns[1]
        self.background_color = columns[2]
        self.font_color = columns[3]
        self.font_name = columns[4]
        self.opacity = columns[5]
        self.x = columns[6]
        self.y = columns[7]
        self.shadow_opacity = columns[8]
        self.height = columns[9]
        self.width = columns[10]
        self.tag = columns[11]
        self.text = columns[12]
        self.text_alignment_mode = columns[13]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from Processor import Processor
from firebase import firebase
from google.cloud import storage

# Upload a local file to a new file to be created in your bucket.
#zebraBlob = bucket.blob('audios/3A0.jpg')
#zebraBlob.upload_from_filename(filename='3A0.jpg')

parser = argparse.ArgumentParser()
#parser.add_argument("-v", "--verbose", help="Mostrar información de depuración", action="store_true")
parser.add_argument("-f", "--file", help="Path of the file to be processed", required=True)
parser.add_argument("-s", "--styles", help="Process styles", action='store_true')
parser.add_argument("-v", "--videos", help="Process videos", action='store_true')
parser.add_argument("-n", "--narrations", help="Process narrations", action='store_true')
parser.add_argument("-a", "--audios", help="Process audios", action='store_true')
parser.add_argument("-t", "--tutorials", help="Process tutorials", action='store_true')
args = parser.parse_args()

processor = Processor(args.file)
if args.styles:
    processor.uploadAdStyles()
if args.videos:
    processor.uploadVideos()
if args.narrations:
    processor.uploadNarrations()
if args.audios:
    processor.uploadAudios()
if args.tutorials:
    processor.uploadTutorials()
#processor.uploadTexts()
class TextOverlayHeader(object):

    def __init__(self, columns):        
        self.configuration_name	= columns[0]
        self.is_last = columns[1]
        self.start_frame = columns[2]
        self.items = []
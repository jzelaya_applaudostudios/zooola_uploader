import json
from HeartBeat import HeartBeat
class BaseClass(object):

    def toArray(self, item):
        if isinstance(item,float):
            item = int(item)
        item = str(item)
        return item.split(",")

    def getHeartBeat(self, heartbeatString):
        print "-->HEAR BEATS STRING: ", heartbeatString
        new_heartbeats = []        
        heartbeats = self.toArray(heartbeatString)
        for heartbeat in heartbeats:
            new_hearbeat = HeartBeat()
            print "--->HEART BEAT ELEMENT", heartbeat
            items = heartbeat.split('|')
            for index, item in enumerate(items):
                print "----->HEART BEAT ITEM", item
                if (index == 0):       
                    new_hearbeat.start_frame = item                
                else:
                    if self.isAsset(item):
                        print "ES DE ASSET"
                        new_hearbeat.A = item.split(':')[1]
                    elif self.isType(item):
                        print "ES DE TIPO"
                        new_hearbeat.T = item.split(':')[1]
                    else:
                        new_hearbeat.video_frame = item
                jsonStr = json.dumps(new_hearbeat.__dict__)
                print jsonStr
                jsonHeartbeat = json.loads(jsonStr)

            new_heartbeats.append(jsonHeartbeat)
        return self.arrayToJson(new_heartbeats)

    def isAsset(self, item):
        if "A:" in item:
            return True
        if "a:" in item:
            return True
        else:
            return False

    def isType(self, item):
        if "T:" in item:
            return True
        if "t:" in item:
            return True
        else:
            return False

    def toJson(self, item):
        items = self.toArray(item)
        return self.arrayToJson(items)

    def arrayToJson(self, items):
        jsonObjects = []
        for item in items:
            jsonStr = json.dumps(item)
            jsonItem = json.loads(jsonStr)
            jsonObjects.append(jsonItem)
        return jsonObjects
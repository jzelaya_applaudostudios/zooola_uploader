from BaseClass import BaseClass
class Tutorial(BaseClass):

    def __init__(self, columns):
        self.date_added = columns[0]
        self.added_by = columns[1]
        self.date_uploaded = columns[2]        
        zooola_name = columns[3].strip('.')
        if isinstance(zooola_name,float):
            zooola_name = int(zooola_name)
        self.zooola_name = zooola_name
        self.display_name = columns[4]
        self.biz_style = self.toJson(columns[5])
        self.length = columns[6]
        self.file_name	= "tutorials/" + columns[7]
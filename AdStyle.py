from BaseClass import BaseClass
class AdStyle(BaseClass):

    def __init__(self, columns):
        self.date_added	= columns[0]
        self.entered_by	= columns[1]
        self.uploaded_date = columns[2]
        self.category = columns[3]
        self.actual_category = columns[4]
        self.style_display_order = columns[5]
        self.style = columns[6]
        self.replace_clip_options = self.toJson(columns[7])
        self.sample_movie_name = "videos/" + columns[8]
        self.display_name = columns[9]
        self.biz_style = self.toJson(columns[10])
        self.tab_image_name	= "adstyles/" + columns[11]
        self.tab_selected_image_name = "adstyles/" + columns[12]
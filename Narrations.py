from BaseClass import BaseClass
class Narrations(BaseClass):

    def __init__ (self, columns):
        self.date_added = columns[0]
        self.enrered_by = columns[1]
        self.uploaded_date = columns[2]
        self.original_name = columns[3]
        self.origineted = columns[4]
        zooola_name = columns[5].strip('.')
        if isinstance(zooola_name,float):
            zooola_name = int(zooola_name)
        self.zooola_name = zooola_name
        self.display_name = columns[6]
        self.biz_style = self.toJson(columns[7])
        self.style = self.toJson(columns[8])
        self.length = columns[9]
        self.file_name	= "narrations/" + columns[10]
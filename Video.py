from BaseClass import BaseClass
class Video(BaseClass):

    def __init__(self, columns):
        self.price	= columns[0]
        self.commercial_use	= columns[1]
        self.model_release	= columns[2]
        self.property_released	= columns[3]
        self.active	= columns[4]
        self.watermarked	= columns[5]
        self.date_added	= columns[6]
        self.entered_by	= columns[7]
        self.originated	= columns[8]
        self.original_name	= columns[9]
        self.file_name	= "videos/" + columns[10]
        zooola_name = columns[11]
        if isinstance(zooola_name,float):
            zooola_name = int(zooola_name)
        self.zooola_name = zooola_name
        self.style = self.toJson(columns[12])
        self.biz_style	= self.toJson(columns[13])
        self.fps	= columns[14]
        self.length	= columns[15]
        self.sound	= columns[16]
        self.locked	= columns[17]
        self.heartbeat	= self.toJson(columns[18])
        self.shot_size	= columns[19]        
        self.clip_type	= columns[20]
        self.keywords	= self.toJson(columns[21])
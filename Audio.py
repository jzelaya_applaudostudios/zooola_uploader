from BaseClass import BaseClass
class Audio(BaseClass):

    def __init__(self, columns):
        self.date_added = columns[0]
        self.entered_by = columns[1]
        self.uploaded_date = columns[2]
        self.original_name = columns[3]
        self.originated = columns[4]
        zooola_name = columns[5].strip('.')
        if isinstance(zooola_name,float):
            zooola_name = int(zooola_name)
        self.zooola_name = zooola_name
        self.display_name = columns[6]
        self.biz_style = self.toJson(columns[7])
        self.style = self.toJson(columns[8])
        self.length = columns[9]
        self.heart_beat = self.getHeartBeat(columns[10])
        self.text_heartbeat = columns[11]
        self.key_words = self.toJson(columns[12])
        self.file_name	= "audios/" + columns[13]